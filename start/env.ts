/*
|--------------------------------------------------------------------------
| Environment variables service
|--------------------------------------------------------------------------
|
| The `Env.create` method creates an instance of the Env service. The
| service validates the environment variables and also cast values
| to JavaScript data types.
|
*/

import { Env } from '@adonisjs/core/env'

export default await Env.create(new URL('../', import.meta.url), {
  NODE_ENV: Env.schema.enum(['development', 'production', 'test'] as const),
  PORT: Env.schema.number(),
  APP_KEY: Env.schema.string(),
  HOST: Env.schema.string({ format: 'host' }),
  LOG_LEVEL: Env.schema.string(),

  /*
  |----------------------------------------------------------
  | Variables for configuring database connection
  |----------------------------------------------------------
  */
  DB_HOST: Env.schema.string({ format: 'host' }),
  DB_PORT: Env.schema.number(),
  DB_USER: Env.schema.string(),
  DB_PASSWORD: Env.schema.string.optional(),
  DB_DATABASE: Env.schema.string(),

  /*
  |
  | Variables for configuring 2Captcha service and api endpoints
  |
  */
  HEADLESS: Env.schema.boolean(),
  CAPTCHA_API_KEY: Env.schema.string(),
  IPVA_MINAS_GERAIS_URL_SOLICITACAO: Env.schema.string(),
  IPVA_SAO_PAULO_URL_SOLICITACAO: Env.schema.string(),
  MULTAS_MINAS_GERAIS_URL_SOLICITACAO: Env.schema.string(),
  SITUACAO_VEICULAR_MINAS_GERAIS_URL_SOLICITACAO: Env.schema.string(),
  MULTAS_DNIT_URL_SOLICITACAO_TOKEN: Env.schema.string(),
  MULTAS_DNIT_URL_SOLICITACAO: Env.schema.string(),
  NADA_CONSTA_URL_SOLICITACAO: Env.schema.string(),
})
