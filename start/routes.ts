/*
|--------------------------------------------------------------------------
| Routes file
|--------------------------------------------------------------------------
|
| The routes file is used for defining the HTTP routes.
|
*/

const IpvaSaoPauloController = () => import('#controllers/ipva_sao_paulo_controller')
const NadaConstaController = () => import('#controllers/nada_consta_controller')
const MultasDnitController = () => import('#controllers/multas_dnit_controller')
const SituacaoVeicularController = () => import('#controllers/situacao_veicular_controller')
const IpvaMinasGeraisController = () => import('#controllers/ipva_minas_gerais_controller')
const MultasMinasGeraisController = () => import('#controllers/multas_minas_gerais_controller')
import router from '@adonisjs/core/services/router'

router.get('/', async () => {
  return {
    hello: 'world',
  }
})

router
  .group(() => {
    router
      .group(() => {
        router
          .group(() => {
            router.get('ipva', [IpvaMinasGeraisController])
            router.get('multas', [MultasMinasGeraisController])
            router.get('situacao-veicular', [SituacaoVeicularController])
          })
          .prefix('minas-gerais')
        router
          .group(() => {
            router.get('ipva', [IpvaSaoPauloController])
          })
          .prefix('sao-paulo')
        router.get('nada-consta', [NadaConstaController])
        router.get('dnit/multas', [MultasDnitController])
      })
      .prefix('v1')
  })
  .prefix('api')
