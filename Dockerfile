FROM node:21-bookworm

RUN apt update -y
RUN apt install libnss3 libatk1.0-0 libatk-bridge2.0-0 libcups2 libgbm1 libasound2 libpangocairo-1.0-0 libxss1 libgtk-3-0 libx11-xcb1 chromium -y


# Set working directory
WORKDIR /app

# Install dependencies
COPY . .
RUN npx puppeteer browsers install chrome

# Build app
RUN node ace build

RUN cp .env.example .env
COPY ./.env ./build

# Expose port
EXPOSE 3333

# Start app
CMD ["node", "./build/bin/server.js"]
