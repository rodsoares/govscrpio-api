import vine from '@vinejs/vine'

export const multasPayloadValidator = vine.compile(
  vine.object({
    renavam: vine.string().trim().maxLength(11),
    placa: vine.string().trim().minLength(5).maxLength(7),
  })
)
