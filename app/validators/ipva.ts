import vine from '@vinejs/vine'

export const ipvaPayloadValidator = vine.compile(
  vine.object({
    renavam: vine.string().trim().minLength(11).maxLength(11),
    ano: vine.number(),
  })
)
