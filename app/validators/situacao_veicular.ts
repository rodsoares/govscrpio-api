import vine from '@vinejs/vine'

export const situacaoVeicularPayloadValidator = vine.compile(
  vine.object({
    chassi: vine.string().trim(),
    placa: vine.string().trim().minLength(5).maxLength(7),
  })
)
