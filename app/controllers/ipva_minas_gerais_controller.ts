import { ipvaPayloadValidator } from '#validators/ipva'
import { HttpContext } from '@adonisjs/core/http'
import IpvaPayloadDto from '../dtos/ipva_payload_dto.js'
import ExtractIpvaMinasGeraisData from '../actions/extract_ipva_minas_gerais_data.js'
import ProcessIpvaMinasGeraisPage from '../actions/process_ipva_minas_gerais_page.js'
import IpvaResponse from '../responses/ipva_response.js'

export default class IpvaMinasGeraisController {
  async handle({ request }: HttpContext) {
    const data = await request.validateUsing(ipvaPayloadValidator)
    const html = await ProcessIpvaMinasGeraisPage.execute(<IpvaPayloadDto>data)
    const extractedData = ExtractIpvaMinasGeraisData.execute(html)
    return IpvaResponse.make(extractedData).toJson()
  }
}
