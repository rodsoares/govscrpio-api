import { HttpContext } from '@adonisjs/core/http'
import { multasPayloadValidator } from '#validators/multas'
import ExtractDnitData from '../actions/extract_dnit_data.js'

export default class MultasDnitController {
  async handle({ request }: HttpContext) {
    const payload = await request.validateUsing(multasPayloadValidator)
    return ExtractDnitData.execute(payload)
  }
}
