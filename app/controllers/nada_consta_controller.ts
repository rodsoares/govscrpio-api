import { HttpContext } from '@adonisjs/core/http'
import { multasPayloadValidator } from '#validators/multas'
import MultasPayloadDto from '../dtos/multas_payload_dto.js'
import ProcessNadaConstaPage from '../actions/process_nada_consta_page.js'
import ExtractNadaConstaData from '../actions/extract_nada_consta_data.js'
import NadaConstaResponse from '../responses/nada_consta_response.js'

export default class NadaConstaController {
  async handle({ request }: HttpContext) {
    const data = await request.validateUsing(multasPayloadValidator)
    const html = await ProcessNadaConstaPage.execute(<MultasPayloadDto>data)
    const extractedData = ExtractNadaConstaData.execute(html)

    return NadaConstaResponse.make(extractedData).toJson()
  }
}
