import { HttpContext } from '@adonisjs/core/http'
import { situacaoVeicularPayloadValidator } from '#validators/situacao_veicular'
import ProcessSituacaoVeicularPage from '../actions/process_situacao_veicular_page.js'
import SituacaoVeicularPayloadDto from '../dtos/situacao_veicular_payload_dto.js'
import SituacaoVeicularResponse from '../responses/situacao_veicular_response.js'

export default class SituacaoVeicularController {
  async handle({ request }: HttpContext) {
    const payload = await request.validateUsing(situacaoVeicularPayloadValidator)
    const data = await ProcessSituacaoVeicularPage.execute(<SituacaoVeicularPayloadDto>payload)

    return SituacaoVeicularResponse.make(data).toJson()
  }
}
