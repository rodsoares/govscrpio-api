import { HttpContext } from '@adonisjs/core/http'
import { multasPayloadValidator } from '#validators/multas'
import MultasPayloadDto from '../dtos/multas_payload_dto.js'
import ProcessIpvaSaoPauloPage from '../actions/process_ipva_sao_paulo_page.js'
import ExtractIpvaSaoPauloData from '../actions/extract_ipva_sao_paulo_data.js'

export default class IpvaSaoPauloController {
  async handle({ request }: HttpContext) {
    const data = await request.validateUsing(multasPayloadValidator)
    const [mainPage, penaltiesPage] = await ProcessIpvaSaoPauloPage.execute(<MultasPayloadDto>data)
    return ExtractIpvaSaoPauloData.execute(mainPage, penaltiesPage)
  }
}
