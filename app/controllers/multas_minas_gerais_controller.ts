import { multasPayloadValidator } from '#validators/multas'
import { HttpContext } from '@adonisjs/core/http'
import ProcessMultasMinasGeraisPage from '../actions/process_multas_minas_gerais_page.js'
import MultasPayloadDto from '../dtos/multas_payload_dto.js'
import MultasResponse from '../responses/multas_response.js'

export default class MultasMinasGeraisController {
  async handle({ request }: HttpContext) {
    const payload = await request.validateUsing(multasPayloadValidator)
    const data = await ProcessMultasMinasGeraisPage.execute(<MultasPayloadDto>payload)

    return MultasResponse.make(data).toJson()
  }
}
