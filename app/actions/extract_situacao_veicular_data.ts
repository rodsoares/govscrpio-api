import * as cheerio from 'cheerio'

export default class ExtractSituacaoVeicularData {
  static execute(html: string): string {
    // @ts-ignore
    const page = cheerio.load(html)
    return page('#content > div.alert').text()
  }
}
