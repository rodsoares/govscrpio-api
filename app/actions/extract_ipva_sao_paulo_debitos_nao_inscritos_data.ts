import * as cheerio from 'cheerio'

export default class ExtractIpvaSaoPauloDebitosNaoInscritosData {
  static execute(page: cheerio.CheerioAPI): Array<Record<string, string>> {
    let ipva: Array<Record<string, string>> = []

    page('#conteudoPaginaPlaceHolder_tbIpvaPend > tbody > tr').each((row: number) => {
      const year: string = page(
        `#conteudoPaginaPlaceHolder_tbIpvaPend > tbody > tr:nth-child(${row + 1}) > td:nth-child(1) > span`
      )?.text()
      const value: string = page(
        `#conteudoPaginaPlaceHolder_tbIpvaPend > tbody > tr:nth-child(${row + 1}) > td:nth-child(4) > span`
      )?.text()
      ipva.push({
        exercicio: year,
        valor: value,
      })
    })

    ipva.shift()

    return ipva
  }
}
