import * as cheerio from 'cheerio'
import PenaltyDto from '../dtos/penalty_dto.js'

export default class ExtractMultasMinasGeraisData {
  static execute(html: string) {
    // @ts-ignore
    const page = cheerio.load(html)

    let penalties: PenaltyDto[] = []

    page('#content > table > tbody > tr').each((index: number) => {
      const sequence = page(
        `#content > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(${1})`
      )?.text()
      const litigation = page(
        `#content > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(${2})`
      )?.text()
      const description = page(
        `#content > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(${3})`
      )?.text()
      const place = page(
        `#content > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(${4})`
      )?.text()
      const value = page(
        `#content > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(${5})`
      )?.text()

      const needsAddPenaltyData = sequence && litigation && description && place && value

      if (needsAddPenaltyData !== null)
        penalties.push(
          ExtractMultasMinasGeraisData.toDto([sequence, litigation, description, place, value])
        )
    })

    return penalties
  }

  private static toDto(data: Array<string>): PenaltyDto {
    return <PenaltyDto>{
      sequence: Number.parseInt(data[0]),
      litigation: data[1],
      description: data[2],
      place: data[3],
      value: data[4],
    }
  }
}
