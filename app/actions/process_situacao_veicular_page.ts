import puppeteer from 'puppeteer-extra'
import RecaptchaPlugin from 'puppeteer-extra-plugin-recaptcha'
import env from '#start/env'
import SituacaoVeicularPayloadDto from '../dtos/situacao_veicular_payload_dto.js'
import ExtractSituacaoVeicularData from './extract_situacao_veicular_data.js'
import ExtractVeiculoData from './extract_veiculo_data.js'
import VehicleSituationDto from '../dtos/vehicle_situation_dto.js'
import ExtractProprietarioVeiculoData from './extract_proprietario_veiculo_data.js'

export default class ProcessSituacaoVeicularPage {
  static async execute(dto: SituacaoVeicularPayloadDto): Promise<VehicleSituationDto> {
    // @ts-ignore
    puppeteer.use(
      // @ts-ignore
      RecaptchaPlugin({
        provider: {
          id: '2captcha',
          token: env.get('CAPTCHA_API_KEY'),
        },
      })
    )

    // @ts-ignore
    return (
      puppeteer
        // @ts-ignore
        .launch({
          headless: env.get('HEADLESS'),
          args: ['--no-sandbox', '--disable-setuid-sandbox'],
          ignoreHTTPSErrors: true,
        })
        .then(async (browser: any) => {
          const page = await browser.newPage()

          await page.setDefaultTimeout(180000)

          await page.goto(env.get('SITUACAO_VEICULAR_MINAS_GERAIS_URL_SOLICITACAO'))

          await page.solveRecaptchas()

          await page.type('#placa', dto.placa)
          await page.type('#chassi', dto.chassi)

          await page.click('#content > form > button')

          await page.waitForNavigation()

          const contentPrimary = await page.content()

          let link = null

          for (const target of [11, 10, 9, 8, 7, 6, 5]) {
            const nextLinkExists = await page
              .$eval(`#content > div:nth-child(${target}) > a`, () => true)
              .catch(() => false)

            if (!nextLinkExists) {
              continue
            }

            link = await page.$eval(
              `#content > div:nth-child(${target}) > a`,
              (el: any) => el.innerText
            )
            if (
              link === 'Visualizar Autos Digitalizados de Multas Pagas de Competência do Detran-MG'
            ) {
              await page.click(`#content > div:nth-child(${target}) > a`)
              await page.waitForNavigation()
              break
            }
          }

          const contentSecondary = await page.content()

          await browser.close()

          return <VehicleSituationDto>{
            situation: ExtractSituacaoVeicularData.execute(contentPrimary),
            vehicle: ExtractVeiculoData.execute(contentSecondary),
            ipva: ExtractProprietarioVeiculoData.execute(contentPrimary),
          }
        })
    )
  }
}
