import * as cheerio from 'cheerio'
import DebitsNadaConstaDto from '../dtos/debits_nada_consta_dto.js'

export default class ExtractNadaConstaData {
  static execute(html: string): DebitsNadaConstaDto[] {
    // @ts-ignore
    const page = cheerio.load(html)

    let debits: DebitsNadaConstaDto[] = []

    page('#formResMultas > table > tbody > tr').each((index: number) => {
      const documentNumber = page(
        `#formResMultas > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(${1}) > a`
      )?.text()
      const rawValue = page(
        `#formResMultas > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(${2}) > strong`
      )?.text()
      const trafficViolation = page(
        `#formResMultas > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(${3}) > strong`
      )?.text()
      const plates = page(
        `#formResMultas > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(${4}) > strong`
      )?.text()
      const uf = page(
        `#formResMultas > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(${5}) > strong`
      )?.text()
      const date = page(
        `#formResMultas > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(${6}) > strong`
      )?.text()
      const aiNumber = page(
        `#formResMultas > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(${7}) > strong`
      )?.text()

      const needsAddDebitData =
        documentNumber && rawValue && trafficViolation && plates && uf && date && aiNumber

      if (needsAddDebitData !== null)
        debits.push(
          ExtractNadaConstaData.toDto([
            documentNumber,
            rawValue,
            trafficViolation,
            plates,
            uf,
            date,
            aiNumber,
          ])
        )
    })

    return debits
  }

  private static toDto(data: Array<string>): DebitsNadaConstaDto {
    return <DebitsNadaConstaDto>{
      documentNumber: data[0],
      rawValue: data[1],
      trafficViolation: data[2],
      plates: data[3],
      uf: data[4],
      date: data[5],
      aiNumber: data[6],
    }
  }
}
