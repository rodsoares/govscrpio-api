import * as cheerio from 'cheerio'

export default class ExtractIpvaSaoPauloTaxaData {
  static execute(page: cheerio.CheerioAPI): Record<string, string> {
    let taxa: Record<string, string> = {}

    page('#conteudoPaginaPlaceHolder_tbTaxasDetalhe > tbody > tr').each((row: number) => {
      const label: string = page(
        `#conteudoPaginaPlaceHolder_tbTaxasDetalhe > tbody > tr:nth-child(${row + 1}) > td:nth-child(1) > span
        `
      )?.text()
      const value: string = page(
        `#conteudoPaginaPlaceHolder_tbTaxasDetalhe > tbody > tr:nth-child(${row + 1}) > td:nth-child(5) > span`
      )?.text()

      if (
        typeof label === 'string' &&
        label.trim().length > 0 &&
        typeof value === 'string' &&
        value.trim().length > 0
      ) {
        taxa[
          label
            .trim()
            .normalize('NFD')
            .replace(/[\u0300-\u036f]/g, '')
            .replace(':', '')
            .replace(/[^a-zA-Z]+/g, ' ')
            .trim()
            .replace(/\W+/g, '_')
            .toLowerCase()
        ] = value.trim()
      }
    })

    return taxa
  }
}
