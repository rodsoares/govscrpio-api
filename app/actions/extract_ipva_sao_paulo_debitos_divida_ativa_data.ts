import * as cheerio from 'cheerio'

export default class ExtractIpvaSaoPauloDebitosDividaAtivaData {
  static execute(page: cheerio.CheerioAPI): Array<Record<string, string>> {
    let ipva: Array<Record<string, string>> = []

    page(
      '#conteudoPaginaPlaceHolder_Panel1 > table:nth-child(20) > tbody > tr > td > table > tbody > tr'
    ).each((row: number) => {
      const year: string = page(
        `#conteudoPaginaPlaceHolder_Panel1 > table:nth-child(20) > tbody > tr > td > table > tbody > tr:nth-child(${row + 1}) > td:nth-child(1) > span`
      )?.text()
      ipva.push({
        exercicio: year,
      })
    })

    ipva.shift()

    return ipva
  }
}
