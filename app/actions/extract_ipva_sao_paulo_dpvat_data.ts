import * as cheerio from 'cheerio'

export default class ExtractIpvaSaoPauloDpvatData {
  static execute(page: cheerio.CheerioAPI): Array<Record<string, string>> {
    let dpvat: Array<Record<string, string>> = []

    page('#conteudoPaginaPlaceHolder_tbDpvats > tbody > tr').each((row: number) => {
      const year: string = page(
        `#conteudoPaginaPlaceHolder_tbDpvats > tbody > tr:nth-child(${row + 1}) > td:nth-child(1) > span`
      )?.text()
      const value: string = page(
        `#conteudoPaginaPlaceHolder_tbDpvats > tbody > tr:nth-child(${row + 1}) > td:nth-child(5) > span`
      )?.text()

      if (year.trim().length > 0) {
        dpvat.push({
          exercicio: year,
          valor: value,
        })
      }
    })

    dpvat.shift()

    return dpvat
  }
}
