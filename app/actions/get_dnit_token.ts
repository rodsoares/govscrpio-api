import MultasPayloadDto from '../dtos/multas_payload_dto.js'
import axios from 'axios'
import env from '#start/env'

export default class GetDnitAuthorizationToken {
  static async execute(dto: MultasPayloadDto): Promise<any> {
    try {
      const headers = {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
      }
      const { data } = await axios.post(env.get('MULTAS_DNIT_URL_SOLICITACAO_TOKEN'), dto, headers)
      return data.token
    } catch (e) {
      return null
    }
  }
}
