import Captcha from '2captcha-ts'
import env from '#start/env'
export default class SolveCloudFlareTurnstileCaptcha {
  static async execute(page: any): Promise<any> {
    const sitekey = await page
      .$eval('.cf-turnstile', (el: any) => el.getAttribute('data-sitekey'))
      .catch(() => null)

    if (sitekey !== null) {
      const solver = new Captcha.Solver(env.get('CAPTCHA_API_KEY'))
      const response = await solver.cloudflareTurnstile({
        pageurl: env.get('IPVA_MINAS_GERAIS_URL_SOLICITACAO'),
        sitekey: sitekey,
      })

      await page.$eval('input[name=cf-turnstile-response]', (el: any) =>
        el.setAttribute('type', '')
      )
      await page.type('input[name=cf-turnstile-response]', response.data)
    }
  }
}
