import puppeteer from 'puppeteer-extra'
import RecaptchaPlugin from 'puppeteer-extra-plugin-recaptcha'
import env from '#start/env'
import MultasPayloadDto from '../dtos/multas_payload_dto.js'

export default class ProcessNadaConstaPage {
  static async execute(dto: MultasPayloadDto): Promise<string> {
    // @ts-ignore
    puppeteer.use(
      // @ts-ignore
      RecaptchaPlugin({
        provider: {
          id: '2captcha',
          token: env.get('CAPTCHA_API_KEY'),
        },
      })
    )

    // @ts-ignore
    return (
      puppeteer
        // @ts-ignore
        .launch({
          headless: env.get('HEADLESS'),
          args: ['--no-sandbox', '--disable-setuid-sandbox'],
          ignoreHTTPSErrors: true,
        })
        .then(async (browser: any) => {
          const page = await browser.newPage()

          await page.setDefaultTimeout(180000)

          await page.goto(env.get('NADA_CONSTA_URL_SOLICITACAO'))

          await page.solveRecaptchas()

          await page.type('#formConsultarExterno\\:placa', dto.placa)
          await page.type('#formConsultarExterno\\:renavam', dto.renavam)

          await page.click('#inputDiv > p > a')

          await page.waitForNavigation()

          const content = await page.content()
          await browser.close()

          return content
        })
    )
  }
}
