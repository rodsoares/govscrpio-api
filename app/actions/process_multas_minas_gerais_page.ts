import env from '#start/env'
import MultasPayloadDto from '../dtos/multas_payload_dto.js'
import puppeteer from 'puppeteer-extra'
import RecaptchaPlugin from 'puppeteer-extra-plugin-recaptcha'
import ExtractMultasMinasGeraisData from './extract_multas_minas_gerais_data.js'

export default class ProcessMultasMinasGeraisPage {
  static async execute(dto: MultasPayloadDto): Promise<Array<object>> {
    // @ts-ignore
    puppeteer.use(
      // @ts-ignore
      RecaptchaPlugin({
        provider: {
          id: '2captcha',
          token: env.get('CAPTCHA_API_KEY'),
        },
      })
    )

    return await puppeteer
      // @ts-ignore
      .launch({
        headless: env.get('HEADLESS'),
        args: ['--no-sandbox', '--disable-setuid-sandbox'],
        ignoreHTTPSErrors: true,
      })
      .then(async (browser: any) => {
        const page = await browser.newPage()

        await page.setDefaultTimeout(180000)

        await page.goto(env.get('MULTAS_MINAS_GERAIS_URL_SOLICITACAO'))

        await page.type('#renavam', dto.renavam)
        await page.type('#placa', dto.placa)

        await page.click('#content > form > button')
        await page.waitForNavigation()

        const invalidInputsData = await page
          .$eval('#content > div.alert.alert-danger.alert-dismissible.font-weight-bold', () => true)
          .catch(() => false)

        const rowsCount = !invalidInputsData
          ? await page.$$eval('#content > table > tbody > tr', (rows: any) => rows.length)
          : 0

        let items: Array<object> = []

        for (let index: number = 0; index < rowsCount; index++) {
          const type = await page.$eval(
            `#content > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(1) > a`,
            (el: any) => el.textContent
          )

          await page.click(
            `#content > table > tbody > tr:nth-child(${index + 1}) > td:nth-child(1) > a`
          )

          await page.waitForNavigation()

          items.push({
            type: type,
            items: ExtractMultasMinasGeraisData.execute(await page.content()),
          })

          // Go back
          await page.click('#content > div:nth-child(8) > a')
          await page.waitForNavigation()
        }

        await browser.close()
        return items
      })
  }
}
