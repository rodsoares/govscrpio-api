import * as cheerio from 'cheerio'
import ExtractIpvaSaoPauloVeiculoData from './extract_ipva_sao_paulo_veiculo_data.js'
import ExtractIpvaSaoPauloIpvaData from './extract_ipva_sao_paulo_ipva_data.js'
import ExtractIpvaSaoPauloPagamentosDeDebitosData from './extract_ipva_sao_paulo_pagamentos_de_debitos_data.js'
import ExtractIpvaSaoPauloPagamentosEfetuadosData from './extract_ipva_sao_paulo_pagamentos_efetuados_data.js'
import ExtractIpvaSaoPauloDebitosNaoInscritosData from './extract_ipva_sao_paulo_debitos_nao_inscritos_data.js'
import ExtractIpvaSaoPauloDpvatData from './extract_ipva_sao_paulo_dpvat_data.js'
import ExtractIpvaSaoPauloDebitosDividaAtivaData from './extract_ipva_sao_paulo_debitos_divida_ativa_data.js'
import ExtractIpvaSaoPauloTaxaData from './extract_ipva_sao_paulo_taxas_data.js'
import ExtractIpvaSaoPauloMultasData from './extract_ipva_sao_paulo_multas_data.js'

export default class ExtractIpvaSaoPauloData {
  static execute(html: string, penaltiesHtml: string) {
    // @ts-ignore
    const page = cheerio.load(html)
    const penaltiesPage = cheerio.load(penaltiesHtml)
    return {
      veiculo: ExtractIpvaSaoPauloVeiculoData.execute(page),
      ipva: ExtractIpvaSaoPauloIpvaData.execute(page),
      pagamentos_de_debitos: ExtractIpvaSaoPauloPagamentosDeDebitosData.execute(page),
      pagamentos_efetuados: ExtractIpvaSaoPauloPagamentosEfetuadosData.execute(page),
      debitos_nao_inscritos: ExtractIpvaSaoPauloDebitosNaoInscritosData.execute(page),
      debitos_divida_ativa: ExtractIpvaSaoPauloDebitosDividaAtivaData.execute(page),
      dpvat: ExtractIpvaSaoPauloDpvatData.execute(page),
      taxas: ExtractIpvaSaoPauloTaxaData.execute(page),
      multas: ExtractIpvaSaoPauloMultasData.execute(penaltiesPage),
    }
  }
}
