import MultasPayloadDto from '../dtos/multas_payload_dto.js'
import GetDnitAuthorizationToken from './get_dnit_token.js'
import GetDnitData from './get_dnit_data.js'

export default class ExtractDnitData {
  static async execute(dto: MultasPayloadDto): Promise<object> {
    const token = await GetDnitAuthorizationToken.execute(dto)
    return token !== null ? await GetDnitData.execute(token) : {}
  }
}
