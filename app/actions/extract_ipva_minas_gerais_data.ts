import * as cheerio from 'cheerio'
import DebitsDto from '../dtos/debits_dto.js'

export default class ExtractIpvaMinasGeraisData {
  static execute(html: string) {
    // @ts-ignore
    const page = cheerio.load(html)

    let debits: DebitsDto[] = []

    page('#tabela-ipva-consolidado > tbody > tr').each((index: number) => {
      const tributeOrInstallment = page(
        `#tabela-ipva-consolidado > tbody > tr:nth-child(${index + 1}) > td:nth-child(${2}) > span`
      )?.text()
      const value = page(
        `#tabela-ipva-consolidado > tbody > tr:nth-child(${index + 1}) > td:nth-child(${3}) > span`
      )?.text()
      const penalty = page(
        `#tabela-ipva-consolidado > tbody > tr:nth-child(${index + 1}) > td:nth-child(${4}) > span`
      )?.text()
      const fees = page(
        `#tabela-ipva-consolidado > tbody > tr:nth-child(${index + 1}) > td:nth-child(${5}) > span`
      )?.text()
      const totalValue = page(
        `#tabela-ipva-consolidado > tbody > tr:nth-child(${index + 1}) > td:nth-child(${6}) > span`
      )?.text()
      const dueDate = page(
        `#tabela-ipva-consolidado > tbody > tr:nth-child(${index + 1}) > td:nth-child(${7}) > span`
      )?.text()

      const needsAddDebitData =
        tributeOrInstallment && value && penalty && fees && totalValue && dueDate

      if (needsAddDebitData !== null)
        debits.push(
          ExtractIpvaMinasGeraisData.toDto([
            tributeOrInstallment,
            value,
            penalty,
            fees,
            totalValue,
            dueDate,
          ])
        )
    })

    return debits
  }

  private static toDto(data: Array<string>): DebitsDto {
    return <DebitsDto>(<unknown>{
      tributeOrInstallment: data[0],
      value: ExtractIpvaMinasGeraisData.sanitize(data[1]),
      penalty: ExtractIpvaMinasGeraisData.sanitize(data[2]),
      fees: ExtractIpvaMinasGeraisData.sanitize(data[3]),
      totalValue: ExtractIpvaMinasGeraisData.sanitize(data[4]),
      dueDate: data[5],
    })
  }

  private static sanitize(rawData: string): string {
    // @ts-ignore
    return rawData
      .replace(',', '.')
      .match(/[0-9]+\.?[0-9]*/g)
      .join('')
  }
}
