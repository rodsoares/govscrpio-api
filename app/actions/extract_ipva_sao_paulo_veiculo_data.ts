import * as cheerio from 'cheerio'

export default class ExtractIpvaSaoPauloVeiculoData {
  static execute(page: cheerio.CheerioAPI): Record<string, string> {
    let vehicle: Record<string, string> = {}

    for (let count = 0; count < 2; count++) {
      const labelColumn = count < 1 ? 1 : 3
      const valueColumn = count < 1 ? 2 : 4

      page(
        '#conteudoPaginaPlaceHolder_Panel1 > table:nth-child(5) > tbody > tr > td > table > tbody > tr'
      ).each((row: number) => {
        const label: string = page(
          `#conteudoPaginaPlaceHolder_Panel1 > table:nth-child(5) > tbody > tr > td > table > tbody > tr:nth-child(${row + 1}) > td:nth-child(${labelColumn}) > span
        `
        )?.text()
        const value: string = page(
          `#conteudoPaginaPlaceHolder_Panel1 > table:nth-child(5) > tbody > tr > td > table > tbody > tr:nth-child(${row + 1}) > td:nth-child(${valueColumn}) > span`
        )?.text()

        if (
          typeof label === 'string' &&
          label.trim().length > 0 &&
          typeof value === 'string' &&
          label.trim().length > 0
        ) {
          vehicle[
            label
              .trim()
              .normalize('NFD')
              .replace(/[\u0300-\u036f]/g, '')
              .replace(':', '')
              .replace(/\W+/g, '_')
              .toLowerCase()
          ] = value.trim()
        }
      })
    }

    return vehicle
  }
}
