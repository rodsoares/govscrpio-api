import * as cheerio from 'cheerio'
import VehicleDto from '../dtos/vehicle_dto.js'

export default class ExtractVeiculoData {
  static execute(html: string): VehicleDto {
    // @ts-ignore
    const page = cheerio.load(html)
    const placa = page('#placa').val()
    const renavam = page('#renavam').val()

    return <VehicleDto>{
      placa: placa,
      renavam: renavam,
    }
  }
}
