import * as cheerio from 'cheerio'

export default class ExtractIpvaSaoPauloPagamentosEfetuadosData {
  static execute(page: cheerio.CheerioAPI): {} {
    let debits: Array<Record<string, string>> = []

    const labels: string[] = []
    const values: string[][] = []

    page('#conteudoPaginaPlaceHolder_tbPagtosEfetuados > tbody > tr:nth-child(2)').each(() => {
      page('#conteudoPaginaPlaceHolder_tbPagtosEfetuados > tbody > tr:nth-child(2) > td').each(
        (column: number) => {
          labels.push(
            page(
              `#conteudoPaginaPlaceHolder_tbPagtosEfetuados > tbody > tr:nth-child(2) > td:nth-child(${column + 1}) > span`
            )?.text()
          )
        }
      )
    })

    page('#conteudoPaginaPlaceHolder_tbPagtosEfetuados > tbody > tr').each((row) => {
      const item = page(
        `#conteudoPaginaPlaceHolder_tbPagtosEfetuados > tbody > tr:nth-child(${row + 1}) > td`
      )
        .map((column: number) => {
          return page(
            `#conteudoPaginaPlaceHolder_tbPagtosEfetuados > tbody > tr:nth-child(${row + 1}) > td:nth-child(${column + 1}) > span`
          )?.text()
        })
        .toArray()
      if (item?.length) values.push(item)
    })

    if (values.length > 2) {
      values.shift()
      values.shift()
    }

    for (const value of values) {
      let debit: Record<string, string> = {}
      for (const [index, label] of labels.entries()) {
        debit[
          label
            .trim()
            .normalize('NFD')
            .replace(/[\u0300-\u036f]/g, '')
            .replace(':', '')
            .replace(/[^a-zA-Z]+/g, ' ')
            .trim()
            .replace(/\W+/g, '_')
            .toLowerCase()
        ] = value[index]
      }
      debits.push(debit)
    }

    return debits
  }
}
