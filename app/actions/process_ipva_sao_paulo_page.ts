import puppeteer from 'puppeteer-extra'
import RecaptchaPlugin from 'puppeteer-extra-plugin-recaptcha'
import env from '#start/env'
import MultasPayloadDto from '../dtos/multas_payload_dto.js'

export default class ProcessIpvaSaoPauloPage {
  static async execute(dto: MultasPayloadDto): Promise<string[]> {
    // @ts-ignore
    puppeteer.use(
      // @ts-ignore
      RecaptchaPlugin({
        provider: {
          id: '2captcha',
          token: env.get('CAPTCHA_API_KEY'),
        },
      })
    )

    return (
      puppeteer
        // @ts-ignore
        .launch({
          headless: env.get('HEADLESS'),
          args: ['--no-sandbox', '--disable-setuid-sandbox'],
          ignoreHTTPSErrors: true,
        })
        .then(async (browser: any) => {
          const page = await browser.newPage()

          await page.setDefaultTimeout(180000)

          await page.goto(env.get('IPVA_SAO_PAULO_URL_SOLICITACAO'))

          await page.type('#conteudoPaginaPlaceHolder_txtRenavam', dto.renavam)
          await page.type('#conteudoPaginaPlaceHolder_txtPlaca', dto.placa)

          await Promise.all([
            page.click('#conteudoPaginaPlaceHolder_btn_Consultar'),
            page.waitForNavigation(),
          ])

          const content = await page.content()

          const penaltiesPageExists = await page
            .$eval('#conteudoPaginaPlaceHolder_btnDetalharMultas', () => true)
            .catch(() => false)

          let penaltiesPage = ''
          if (penaltiesPageExists) {
            await Promise.all([
              page.click('#conteudoPaginaPlaceHolder_btnDetalharMultas'),
              page.waitForNavigation(),
            ])
            penaltiesPage = await page.content()
          }

          await browser.close()

          return [content, penaltiesPage]
        })
    )
  }
}
