import * as cheerio from 'cheerio'

export default class ExtractIpvaSaoPauloIpvaData {
  static execute(page: cheerio.CheerioAPI): Record<string, string> {
    let ipva: Record<string, string> = {}

    page(
      '#conteudoPaginaPlaceHolder_Panel1 > table:nth-child(11) > tbody > tr > td > table > tbody > tr'
    ).each((row: number) => {
      const label: string = page(
        `#conteudoPaginaPlaceHolder_Panel1 > table:nth-child(11) > tbody > tr > td > table > tbody > tr:nth-child(${row + 1}) > td:nth-child(1) > span
        `
      )?.text()
      const value: string = page(
        `#conteudoPaginaPlaceHolder_Panel1 > table:nth-child(11) > tbody > tr > td > table > tbody > tr:nth-child(${row + 1}) > td:nth-child(4) > span`
      )?.text()

      if (
        typeof label === 'string' &&
        label.trim().length > 0 &&
        typeof value === 'string' &&
        label.trim().length > 0
      ) {
        ipva[
          label
            .trim()
            .normalize('NFD')
            .replace(/[\u0300-\u036f]/g, '')
            .replace(':', '')
            .replace(/[^a-zA-Z]+/g, ' ')
            .trim()
            .replace(/\W+/g, '_')
            .toLowerCase()
        ] = value.trim()
      }
    })

    if (Object.keys(ipva).length) {
      const keys = Object.keys(ipva)
      const lastKey = keys[keys.length - 1]
      delete ipva[lastKey]
    }
    return ipva
  }
}
