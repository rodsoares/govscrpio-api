import puppeteer from 'puppeteer-extra'
import RecaptchaPlugin from 'puppeteer-extra-plugin-recaptcha'
import env from '#start/env'
import IpvaPayloadDto from '../dtos/ipva_payload_dto.js'
import SolveCloudFlareTurnstileCaptcha from './solvers/solve_cloudflare_turnstile_captcha.js'

export default class ProcessIpvaMinasGeraisPage {
  static async execute(dto: IpvaPayloadDto): Promise<string> {
    // @ts-ignore
    puppeteer.use(
      // @ts-ignore
      RecaptchaPlugin({
        provider: {
          id: '2captcha',
          token: env.get('CAPTCHA_API_KEY'),
        },
      })
    )

    return (
      puppeteer
        // @ts-ignore
        .launch({
          headless: env.get('HEADLESS'),
          args: ['--no-sandbox', '--disable-setuid-sandbox'],
          ignoreHTTPSErrors: true,
        })
        .then(async (browser: any) => {
          const page = await browser.newPage()

          await page.setDefaultTimeout(180000)

          await page.goto(env.get('IPVA_MINAS_GERAIS_URL_SOLICITACAO'))

          await page.solveRecaptchas() // mudou para o cloudfare captcha :stonks:
          await SolveCloudFlareTurnstileCaptcha.execute(page)

          await page.type('#renavam', dto.renavam)
          await page.click('#anoExercicio-button')

          const yearIndex = new Date().getFullYear() + 1 - dto.ano + 1
          await page.click(`#anoExercicio-menu > li:nth-child(${yearIndex})`)

          await page.click('#submit')

          await page.waitForNavigation()

          const content = await page.content()
          await browser.close()

          return content
        })
    )
  }
}
