import * as cheerio from 'cheerio'

export default class ExtractIpvaSaoPauloMultasData {
  static execute(page: cheerio.CheerioAPI): any {
    let data: any = []

    page(`#conteudoPaginaPlaceHolder_tbMultasDet > tbody > tr`).each((row: number) => {
      page(`#conteudoPaginaPlaceHolder_tbMultasDet > tbody > tr:nth-child(${row + 1}) > td`).each(
        (column: number) => {
          const value = page(
            `#conteudoPaginaPlaceHolder_tbMultasDet > tbody > tr:nth-child(${row + 1}) > td:nth-child(${column + 1}) > span`
          )?.text()
          data.push(value?.trim())
        }
      )
    })

    data = data.filter((item: string) => item !== '')

    let penalties: any = []
    let item: any = {}

    const labels = data.filter((label: string) => label.trim().slice(-1) === ':')
    const values = data.filter((value: string) => value.trim().slice(-1) !== ':')

    for (const [count, label] of labels.entries()) {
      item[label.replace(':', '')] = values[count]

      if (label === 'Receita:') {
        penalties.push({ ...item })
      }
    }

    return penalties
  }
}
