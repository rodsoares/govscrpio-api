import axios from 'axios'
import env from '#start/env'

export default class GetDnitData {
  static async execute(token: string): Promise<any> {
    try {
      const headers = {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
        },
      }
      const result = await axios.get(env.get('MULTAS_DNIT_URL_SOLICITACAO'), headers)
      return result.data
    } catch (e) {
      return null
    }
  }
}
