import * as cheerio from 'cheerio'

export default class ExtractIpvaSaoPauloPagamentosDeDebitosData {
  static execute(page: cheerio.CheerioAPI): Array<Record<string, string>> {
    let debits: Array<Record<string, string>> = []

    page(
      '#conteudoPaginaPlaceHolder_pnlModalidadesPagto > table:nth-child(2) > tbody > tr > td > table > tbody > tr'
    ).each((row: number) => {
      const label: string = page(
        `#conteudoPaginaPlaceHolder_pnlModalidadesPagto > table:nth-child(2) > tbody > tr > td > table > tbody > tr:nth-child(${row + 1}) > td:nth-child(1) > span
        `
      )?.text()
      const dueDate: string = page(
        `#conteudoPaginaPlaceHolder_pnlModalidadesPagto > table:nth-child(2) > tbody > tr > td > table > tbody > tr:nth-child(${row + 1}) > td:nth-child(2) > span
        `
      )?.text()
      const total: string = page(
        `#conteudoPaginaPlaceHolder_pnlModalidadesPagto > table:nth-child(2) > tbody > tr > td > table > tbody > tr:nth-child(${row + 1}) > td:nth-child(5) > span
        `
      )?.text()

      if (
        typeof label === 'string' &&
        label.trim().length > 0 &&
        typeof dueDate === 'string' &&
        dueDate.trim().length > 0 &&
        typeof total === 'string' &&
        total.trim().length > 0
      ) {
        debits.push({
          tipo: label.trim(),
          data_vencimento: dueDate.trim(),
          valor: total.trim(),
        })
      }
    })

    debits.shift()

    return debits
  }
}
