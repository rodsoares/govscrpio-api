import * as cheerio from 'cheerio'

export default class ExtractProprietarioVeiculoData {
  static execute(html: string): any {
    // @ts-ignore
    const page = cheerio.load(html)
    return page(`#content > dl > dd:nth-child(2)`)?.text()
  }
}
