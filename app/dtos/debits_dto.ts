export default interface DebitsDto {
  tributeOrInstallment: string
  value: string
  fees: string
  totalValue: string
  penalty: string
  dueDate: string
}
