export default interface IpvaPayloadDto {
  renavam: string
  ano: number
}
