import VehicleDto from './vehicle_dto.js'

export default interface VehicleSituationDto {
  situation: string
  vehicle: VehicleDto
  ipva: string
}
