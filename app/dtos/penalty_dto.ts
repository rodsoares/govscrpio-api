export default interface PenaltyDto {
  sequence: number
  litigation: string
  description: string
  place: string
  value: string
}
