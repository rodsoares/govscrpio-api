export default interface SituacaoVeicularPayloadDto {
  chassi: string
  placa: string
}
