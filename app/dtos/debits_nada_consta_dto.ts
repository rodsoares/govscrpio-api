export default interface DebitsNadaConstaDto {
  documentNumber: string
  rawValue: string
  trafficViolation: string
  plates: string
  uf: string
  date: string
  aiNumber: string
}
