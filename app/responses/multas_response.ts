export default class MultasResponse {
  constructor(
    private penalties: Array<{
      type: any
      items: any
    }>
  ) {}
  static make(penalties: Array<{}>): MultasResponse {
    return new MultasResponse(<any>penalties)
  }

  toJson() {
    let multas: Array<{}> = []

    for (const item of this.penalties) {
      let items: Array<any> = []

      for (const debit of item.items) {
        items.push({
          sequencia: debit.sequence,
          processo: debit.litigation,
          descricao: debit.description,
          local: debit.place,
          valor: debit.value,
        })
      }

      multas.push({
        tipo: item.type,
        items: items,
      })
    }

    return { error: false, multas: multas }
  }
}
