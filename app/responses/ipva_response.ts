import DebitsDto from '../dtos/debits_dto.js'

export default class IpvaResponse {
  constructor(private debits: DebitsDto[]) {}
  static make(debits: DebitsDto[]): IpvaResponse {
    return new IpvaResponse(debits)
  }

  toJson() {
    let debitos: Array<{}> = []

    for (const item of this.debits) {
      debitos.push({
        tributo_ou_parcela: item.tributeOrInstallment,
        receita: Number.parseFloat(item.value),
        multa: Number.parseFloat(item.penalty),
        juros: Number.parseFloat(item.fees),
        valor_total: Number.parseFloat(item.totalValue),
        data_de_vencimento: item.dueDate,
      })
    }

    return { error: false, debitos: debitos }
  }
}
