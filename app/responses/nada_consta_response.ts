import DebitsNadaConstaDto from '../dtos/debits_nada_consta_dto.js'

export default class NadaConstaResponse {
  constructor(private debits: DebitsNadaConstaDto[]) {}
  static make(debits: DebitsNadaConstaDto[]): NadaConstaResponse {
    return new NadaConstaResponse(debits)
  }

  toJson() {
    let debitos: Array<{}> = []

    for (const item of this.debits) {
      debitos.push({
        numero_documento: item.documentNumber,
        valor: item.rawValue,
        violacao: item.trafficViolation,
        placa: item.plates,
        uf: item.uf,
        data_da_infracao: item.date,
        numero_ai: item.aiNumber,
      })
    }

    return { error: false, debitos: debitos }
  }
}
