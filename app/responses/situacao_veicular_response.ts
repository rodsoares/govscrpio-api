import VehicleSituationDto from '../dtos/vehicle_situation_dto.js'

export default class SituacaoVeicularResponse {
  constructor(private situation: VehicleSituationDto) {}

  static make(situation: VehicleSituationDto): SituacaoVeicularResponse {
    return new SituacaoVeicularResponse(situation)
  }

  toJson() {
    return {
      error: false,
      situacao_veicular: {
        situacao: this.situation.situation,
        placa: this.situation.vehicle.placa,
        renavam: this.situation.vehicle.renavam,
        ipva_pago: this.situation.ipva,
      },
    }
  }
}
