#!/bin/bash

# Add any other necessary commands
echo "Pulling"
git fetch --all && git pull --rebase

echo "Downloading npm dependencies"
npm install && npm update

echo "Building application"
docker-compose down -v && docker-compose up -d --build

# Exit script successfully
exit 0
